const express = require('express')

const app = express()

// variables d'env
const PORT = process.env.SERVER_PORT || 3000;

// endpoints (routes)
app.get('/', function (req, res) {
  res.send('Hello World')
})

app.get('/help', function (req, res) {
  res.send('help!')
})


app.listen(PORT, () => {
  console.log('Server running on port ' + PORT + '...')
})
